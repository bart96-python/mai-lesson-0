# MAI Lesson 0 – Exchange test
## Тестовое задание для опредения уровня знания языка

#### Info
- [Repl.it](https://repl.it) - онлайн IDE для программирования
- [Notepad++](https://notepad-plus-plus.org) (Windows)
- [Notepadqq](https://notepadqq.com/wp) (Linux)
- [Komodo Edit](https://www.activestate.com/komodo-edit) (все платформы)
